/*
 * Copyright 2016-2019 Jonathan Machen {@literal <jonathan.machen@robotaccomplice.com>}.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jais.exceptions;

import java.io.Serial;

/**
 *
 * @author Jonathan Machen {@literal <jonathan.machen@robotaccomplice.com>}
 */
public class InvalidAISCharacterException extends AISException {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     *
     * @param message the message to include with the exception
     */
    public InvalidAISCharacterException(String message) { super(message); }

    /**
     *
     * @param message the message to include with the exception
     * @param t the throwable to include with the exception
     */
    public InvalidAISCharacterException(String message, Throwable t) {
        super(message, t);
        super.setStackTrace(t.getStackTrace());
    }
}
