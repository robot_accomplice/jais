/*
 * Copyright 2016-2019 Jonathan Machen {@literal <jonathan.machen@robotaccomplice.com>}.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jais.messages;

import jais.AISSentence;
import jais.DestinationPort;
import jais.messages.enums.AISMessageType;
import jais.messages.enums.FieldMap;
import jais.messages.enums.MMSIType;
import jais.messages.enums.PortAction;
import lombok.Getter;

import java.time.ZoneOffset;
import org.locationtech.spatial4j.shape.Point;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Jonathan Machen {@literal <jonathan.machen@robotaccomplice.com>}
 */
public interface AISMessage {

    int DEFAULT_RATE_OF_TURN = -128;
    float DEFAULT_SPEED_OVER_GROUND = 1023f;
    int DEFAULT_HEADING = 511;
    int DEFAULT_COURSE_OVER_GROUND = 3600;
    float DEFAULT_LATITUDE = 91;
    float DEFAULT_LONGITUDE = 181;

    /**
     *
     * @param mmsi the MMSI
     * @return a boolean indicating whether the MMSI is valid
     */
    static boolean isValidMmsi(long mmsi) {
        return ((mmsi < 800000000) && (mmsi > 199999999));
    }

    /**
     * Determines whether this is a valid IMO according to NMEA standards
     * 
     * @param imo the IMO (in String form) of the originating vessel
     * @return a boolean indicating whether the provided IMO is valid
     */
    static boolean isValidImo(String imo) {
        if (imo.toLowerCase().startsWith("imo"))
            return isValidImo(Long.parseLong(imo.substring(4)));
        return isValidImo(Long.parseLong(imo));
    }

    /**
     * Determines whether this is a valid IMO according to NMEA standards
     * 
     * @param imo the IMO (in long form) of the originating vessel
     * @return a boolean indicating whether the provided IMO is valid
     */
    static boolean isValidImo(long imo) {
        String intString = Long.toString(imo);

        if (intString.length() != 7) return false;

        int d = 0;
        Integer[] digits = new Integer[7];
        for (char c : intString.toCharArray()) {
            digits[d] = Integer.valueOf(String.valueOf(c));
            d++;
        }

        digits[0] *= 7;
        digits[1] *= 6;
        digits[2] *= 5;
        digits[3] *= 4;
        digits[4] *= 3;
        digits[5] *= 2;

        int sum = 0;
        for (int i = 0; i < 6; i++)
            sum += digits[i];

        return (sum % 10 == digits[6]);
    }

    /**
     *
     * @return a boolean indicating whether the position information is valid
     */
    static boolean isValidPosition(double lat, double lon) {
        return ((lon >= -180 && lon < (AISMessage.DEFAULT_LONGITUDE - 1)) && (lat >= -90 && lat < (AISMessage.DEFAULT_LATITUDE - 1)));
    }

    static boolean isDefaultPosition(double lat, double lon) {
        return lat == AISMessage.DEFAULT_LONGITUDE -1 && lon == AISMessage.DEFAULT_LATITUDE -1;
    }

    /**
     * @param courseOverGround the value to evaluate
     * @return a boolean indicating whether the course information is valid or not
     */
    static boolean isValidCourse(float courseOverGround) {
        return (courseOverGround >= 0 && courseOverGround < 360) || courseOverGround == AISMessage.DEFAULT_COURSE_OVER_GROUND;
    }

    /**
     * @param speed  the value to evaluate
     * @return a boolean indicating whether the speed information is valid or not
     */
    static boolean isValidSpeed(float speed) {
        return (speed >= 0 && speed <= 102.2f) || speed == AISMessage.DEFAULT_SPEED_OVER_GROUND;
    }

    /**
     * @param heading  the value to evaluate
     * @return a boolean indicating whether the heading information is valid or not
     */
    static boolean isValidHeading(int heading) {
        return (heading >= 0 && heading < 360) || heading == AISMessage.DEFAULT_HEADING;
    }

    /**
     * @param rateOfTurn  the value to evaluate
     * @return a boolean indicating whether the rate of turn information is valid or not
     */
    static boolean isValidTurn(int rateOfTurn) {
        return rateOfTurn >= AISMessage.DEFAULT_RATE_OF_TURN && rateOfTurn <= 127;
    }

    /**
     *
     * @param destination decodes encoded destinations
     * @return a List of DestinationPorts as decoded from the destination string
     */
    static List<DestinationPort> decodeDestination(String destination) {
        final List<DestinationPort> movements = new ArrayList<>();
        final String regexStr = "(([A-Z]{2})*(\\^?)([A-Z0-9]{3,4})([<>]*))";
        final Pattern re = Pattern.compile(regexStr);
        final Matcher m = re.matcher(destination);

        // Group 2 is the UNLOCODE
        // Group 3 if present indicates GUID
        // Group 4 is the Port code or GUID
        // Group 5 is the action

        while(m.find()) {
            movements.add(new DestinationPort(m.group(2), m.group(4), PortAction.getForSymbol(m.group(5))));
        }

        return movements;
    }

    /**
     *
     * @return a String containing the source of the message
     */
    String getSource();

    /**
     *
     * @param source a String containing the source of the message
     */
    void setSource(String source);

    /**
     *
     * @return the array of Sentences from which this message was composed
     */
    AISSentence[] getSentences();

    /**
     *
     * @return type AISMessageType of the message
     */
    AISMessageType getType();

    /**
     *
     * @return The map of fields for this message type
     */
    FieldMap[] getFieldMap();

    /**
     * @param offset the timezone offset for which we want the time received to be
     *               returned
     * @return a ZonedDateTime object generated using the time received and provided
     *         ZoneOffset
     */
    ZonedDateTime getTimeReceived(ZoneOffset offset);

    /**
     *
     * @return the time received in its unaltered long value
     */
    long getTimeReceived();

    /**
     *
     * @param mType the AISMessageType of this message
     */
    void setType(AISMessageType mType);

    /**
     *
     * @return the "repeat" value of the message
     */
    int getRepeat();

    /**
     *
     * @return the MMSI of the vessel that sent this message
     */
    int getMmsi();

    /**
     *
     * @return the type of MMSI
     */
    MMSIType getMMSIType();

    /**
     *
     * @return a boolean indicating whether the MMSI is valid
     */
    boolean hasValidMmsi();

    /**
     *
     * @return a boolean indicating whether this message contains positional
     *         data
     */
    boolean hasPosition();

    /**
     *
     * @return the position as a Point
     */
    Point getPosition();

    /**
     *
     * @return whether there is a subtype for the current message
     */
    boolean hasSubType();

    /**
     *
     * @return A properly typed instance of AISMessage given the message content
     *         (usually because of a decoding or parsing error)
     */
    AISMessage getSubTypeInstance();

    /**
     * Decodes this message
     * 
     */
    void decode();

    /**
     * Fields common to all messages
     */
    @Getter
    enum AISFieldMap implements FieldMap {

        TYPE(0, 5),
        REPEAT(6, 7),
        MMSI(8, 37);

        private final int startBit;
        private final int endBit;

        /**
         *
         * @param startBit int
         * @param endBit   int
         */
        AISFieldMap(int startBit, int endBit) {
            this.startBit = startBit;
            this.endBit = endBit;
        }
    }
}
