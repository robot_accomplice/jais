/*
 * Copyright 2016-2019 Jonathan Machen <jonathan.machen@robotaccomplice.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jais.messages;

import jais.messages.enums.AISMessageType;
import jais.AISSentence;

/**
 *
 * @author Jonathan Machen {@literal <jonathan.machen@robotaccomplice.com>}
 */
public class PositionReportClassAResponseToInterrogation extends PositionReportBase {

    /**
     * 
     * @param source
     * @param sentences
     */
    public PositionReportClassAResponseToInterrogation(String source, AISSentence... sentences) {
        super(source, AISMessageType.POSITION_REPORT_CLASS_A_RESPONSE_TO_INTERROGATION, sentences);
    }
}
