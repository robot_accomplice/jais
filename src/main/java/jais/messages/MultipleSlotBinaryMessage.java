/*
 * Copyright 2016-2019 Jonathan Machen <jonathan.machen@robotaccomplice.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jais.messages;

import jais.AISSentence;
import jais.messages.enums.FieldMap;
import lombok.Getter;
import lombok.Setter;
import jais.messages.enums.AISMessageType;
import java.util.BitSet;

/**
 *
 * @author Jonathan Machen {@literal <jonathan.machen@robotaccomplice.com>}
 */
@Getter
@Setter
public class MultipleSlotBinaryMessage extends AISMessageBase {

    private boolean addressed;
    private boolean structured;
    private int destMmsi;
    private int dac;
    private int fid;
    private BitSet data;
    private int radio;

    /**
     *
     * @param source The name of the source of the AISSentence(s)
     * @param sentences the AISSentences from which this message should be composed
     */
    public MultipleSlotBinaryMessage(String source, AISSentence... sentences) {
        super(source, sentences);
    }

    /**
     *
     * @param source The name of the source of the AISSentence(s)
     * @param type the AISMessageType of the message
     * @param sentences the AISSentences from which this message should be composed
     */
    public MultipleSlotBinaryMessage(String source, AISMessageType type, AISSentence... sentences) {
        super(source, type, sentences);
    }

    /**
     *
     */
    @Override
    public final void decode() {
        super.decode();

        for (MultipleSlotBinaryMessageFieldMap field : MultipleSlotBinaryMessageFieldMap.values()) {
            if (bits.size() > field.getEndBit() || field.getEndBit() == -1) {
                switch (field) {
                    case ADDRESSED -> addressed = bits.get(field.getStartBit());
                    case STRUCTURED -> structured = bits.get(field.getStartBit());
                    case DESTINATION_MMSI -> {
                        if (addressed) {
                            destMmsi = AISMessageDecoder.decodeUnsignedInt(bits, field.getStartBit(),
                                    field.getEndBit());
                        }
                    }
                    case DAC -> {
                        if (structured) {
                            dac = AISMessageDecoder.decodeUnsignedInt(bits, field.getStartBit(), field.getEndBit());
                        }
                    }
                    case FID -> {
                        if (structured) {
                            fid = AISMessageDecoder.decodeUnsignedInt(bits, field.getStartBit(), field.getEndBit());
                        }
                    }
                    case DATA -> {
                        if (addressed && bits.length() >= 160) {
                            data = bits.get(70, (bits.size() - 90));
                        } else if (structured && bits.length() >= 166) {
                            data = bits.get(56, (bits.size() - 76));
                        } else if (bits.length() > 61) {
                            data = bits.get(40, bits.size());
                        }
                    }
                    case RADIO -> radio = AISMessageDecoder.decodeUnsignedInt(bits,
                            bits.size() - 21, bits.size() + 1);
                }
            }
        }
    }

    /**
     *
     */
    @Getter
    private enum MultipleSlotBinaryMessageFieldMap implements FieldMap {

        ADDRESSED(38, 38),
        STRUCTURED(39, 39),
        DESTINATION_MMSI(40, 70),
        DAC(40, 50), // depends on the value of structured
        FID(50, 56), // depends on the value of structured
        DATA(-1, -1), // could be anywhere from 0-1004 bits
        RADIO(-1, 20);

        private final int startBit;
        private final int endBit;

        /**
         *
         * @param startBit The first bit to include for decoding of the current field
         * @param endBit The last bit to include for decoding of the current field
         */
        MultipleSlotBinaryMessageFieldMap(int startBit, int endBit) {
            this.startBit = startBit;
            this.endBit = endBit;
        }
    }
}
