/*
 * Copyright 2016-2019 Jonathan Machen <jonathan.machen@robotaccomplice.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jais.messages.enums;

/**
 *
 * @author vermi
 */
public enum Talker {

    AB("Independent AIS Base Station"),
    AD("Dependent AIS Base Station"),
    AG("Heading Track Controller (General)"),
    AP("Heading Track Controller (Magnetic)"),
    AI("Mobile Class A or B AIS Station"),
    AN("AIS Aid to Navigation Station"),
    AR("AIS Receiving Station"),
    AS("AIS Station (ITU_R M1371 - Limited Base Station)"),
    AT("AIS Transmitting Station"),
    AX("AIS Simplex Repeater Station"),
    B2("Blockchain Based Vehicular Data"),
    BI("Bilge Systems"),
    BN("Bridge Navigational Watch Alarm System"),
    BS("Unknown Talker Type"),

    // Communiciations
    CD("Communication - Digital Selective Calling (DSC)"),
    CR("Communication - Data Receiver"),
    CS("Communication - Satellite"),
    CT("Communication - Radio-Telephone (MF/HF)"),
    CV("Communication - Radio-Telephone (VHF)"),
    CX("Communication - Scanning Receiver"),

    DF("Direction Finder"),
    DU("Duplex Repeater Station"),
    EC("Electronic Chart System (ECS)"),
    EI("Electronic Chart Display & Information System (ECDIS)"),
    EP("Emergency Position Indicating Beacon (EPIRB)"),
    ER("Engine Room Monitoring Systems"),
    FD("Fire Door Controller/Monitoring Point"),
    FE("Fire Extinguisher System"),
    FR("Fire Detection Point"),
    FS("Fire Sprinkler System"),

    GA("Galileo"),
    GL("GLONASS Receiver"),
    GN("Global Navigation Satellite System (GNSS)"),
    GP("GPS"),

    // Heading Sensors
    HC("Heading - Compass, Magnetic"),
    HE("Heading - Gyro, North Seeking"),
    HF("Heading - Fluxgate"),
    HN("Heading - Gyro, Non-North Seeking"),

    HD("Hull Door Controller/Monitoring Panel"),
    HS("Hull Stress Monitoring"),
    II("Integrated Instrumentation"),
    IN("Integrated Navigation"),
    LC("Loran C"),
    MX("Multiplexer"),
    NL("Navigation Light Controller"),
    P("Proprietary Code"),
    RA("Radar and/or Radar Plotting"),
    RC("Propulsion Machinery Including Remote Control"),
    SA("Physical Shore AIS Station"),
    SD("Sounder, depth"),
    SG("Steering Gear/Steering Engine"),
    SN("Electronic Positioning System, other/general"),
    SS("Sounder, scanning"),
    TI("Turn Rate Indicator"),
    UP("Microprocessor Controller"),
    U0("User configured talker identifier (0)"),
    U1("User configured talker identifier (1)"),
    U2("User configured talker identifier (2)"),
    U3("User configured talker identifier (3)"),
    U4("User configured talker identifier (4)"),
    U5("User configured talker identifier (5)"),
    U6("User configured talker identifier (6)"),
    U7("User configured talker identifier (7)"),
    U8("User configured talker identifier (8)"),
    U9("User configured talker identifier (9)"),

    // Velocity Sensors
    VD("Velocity - Doppler, other/general"),
    VM("Velocity - Speed Log, Water, Magnetic"),
    VW("Velocity - Speed Log, Water, Mechanical"),

    VR("Voyage Data Recorder"),
    WD("Watertight Door Controller/Monitoring Panel"),
    WI("Weather Instruments"),
    WL("Water Level Detection Systems"),
    YX("Transducer"),

    // Timekeepers, Time/Date
    ZA("Atomics Clock"),
    ZC("Chronometer"),
    ZQ("Quartz"),
    ZV("Radio Update");

    public final String description;

    /**
     * 
     * @param description the long name/description of this Talker
     */
    Talker(String description) {
        this.description = description;
    }

    /**
     * 
     * @param code the short code for a Talker
     * @return a boolean indicating whether or not the talker is known
     */
    public static boolean isKnown(String code) {
        if (code == null)
            return false;

        for (Talker t: Talker.values()) {
            if (t.name().equals(code))
                return true;
        }

        return false;
    }
}
