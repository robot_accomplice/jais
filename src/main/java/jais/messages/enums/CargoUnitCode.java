/*
 * Copyright 2016-2019 Jonathan Machen <jonathan.machen@robotaccomplice.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jais.messages.enums;

import lombok.Getter;

/**
 *
 * @author Jonathan Machen {@literal <jonathan.machen@robotaccomplice.com>}
 */
@Getter
public enum CargoUnitCode {

    DEFAULT(0, "Not available (default)"),
    KILOGRAMS(1, "Kilograms"),
    METRIC_TONS(2, "Metric tons"),
    METRIC_KILOTONS(3, "Metric kilotons");

    private final int code;
    private final String description;

    /**
     * 
     * @param code the numeric code defined by NMEA for the CargoUnit
     * @param description the NMEA-provided description for the CargoUnit
     */
    CargoUnitCode(int code, String description) {
        this.code = code;
        this.description = description;
    }

    /**
     * 
     * @param code the numeric code we wish to use for retrieval
     * @return the CargoUnitCode if there is a match, or null if there isn't
     */
    public static CargoUnitCode getForCode(int code) {
        for (CargoUnitCode type : CargoUnitCode.values()) {
            if (type.getCode() == code)
                return type;
        }

        return null;
    }
}
